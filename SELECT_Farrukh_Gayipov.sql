--Task 1
--Which staff members made the highest revenue for each store and deserve a bonus for the year 2017?

--Solution 1
select 
s.first_name || ' ' || s.last_name as staff_name,
sum(p.amount) as revenue
from
	payment as p
	join staff as s
		on s.staff_id = p.staff_id
where extract (YEAR from p.payment_date) = 2017
group by s.first_name || ' ' || s.last_name
order by sum(p.amount) desc
limit 1;


--Solution 2
WITH RankedRevenue AS (
    SELECT
        s.first_name || ' ' || s.last_name AS staff_name,
        SUM(p.amount) AS revenue,
        RANK() OVER (ORDER BY SUM(p.amount) DESC) AS rnk
    FROM
        payment AS p
        JOIN staff AS s ON s.staff_id = p.staff_id
    WHERE
        EXTRACT(YEAR FROM p.payment_date) = 2017
    GROUP BY
        s.first_name || ' ' || s.last_name
)
SELECT
    staff_name,
    revenue
FROM
    RankedRevenue
WHERE
    rnk = 1;
SELECT
    title,
    rating,
    popularity
from (
    SELECT
        f.title,
        f.rating,
        count(i.film_id) AS popularity,
        row_number() OVER (ORDER BY count(i.film_id) DESC) AS rnk
    from
        inventory AS i
        JOIN rental AS r ON r.inventory_id = i.inventory_id
        JOIN film AS f ON f.film_id = i.film_id
    GROUP BY
        f.title, f.rating
) AS ranked
WHERE rnk <= 5;

--Task 2
--Which five movies were rented more than the others, and what is the expected age of the audience for these movies?

--Solution 1
SELECT
    title,
    rating,
    popularity
from (
    SELECT
        f.title,
        f.rating,
        count(i.film_id) AS popularity,
        row_number() OVER (ORDER BY count(i.film_id) DESC) AS rnk
    from
        inventory AS i
        JOIN rental AS r ON r.inventory_id = i.inventory_id
        JOIN film AS f ON f.film_id = i.film_id
    GROUP BY
        f.title, f.rating
) AS ranked
WHERE rnk <= 5;

--Solution 2
select
	f.title,
	f.rating,
	count(i.film_id) as popularity
from
	inventory as i
	join rental as r
		on r.inventory_id = i.inventory_id 
	join film as f
		on f.film_id = i.film_id 
group by 
	f.title, f.rating
order by 
	count(i.film_id) desc
limit 5;

--Task 3
--Which actors/actresses didn't act for a longer period of time than the others?

--Solution 1
WITH ActorsPeriodWithoutActing AS (
    SELECT
        a.first_name || ' ' || a.last_name AS actors_name,
        MAX(f.release_year) AS latest_film_year,
        2023 - MAX(f.release_year) AS years_without_acting
    FROM actor as a
    LEFT JOIN film_actor as fa ON a.actor_id = fa.actor_id
    LEFT JOIN film as f ON fa.film_id = f.film_id
    GROUP BY a.actor_id
)
SELECT
	actors_name,
	years_without_acting,
	latest_film_year
FROM ActorsPeriodWithoutActing
ORDER BY years_without_acting DESC
limit 4;

--Solution 2
WITH ActorsPeriodWithoutActing AS (
    SELECT
        a.first_name || ' ' || a.last_name AS actors_name,
        MAX(f.release_year) AS latest_film_year,
        2023 - MAX(f.release_year) AS years_without_acting,
        RANK() OVER (ORDER BY 2023 - MAX(f.release_year) DESC) AS rnk
    FROM actor AS a
    LEFT JOIN film_actor AS fa ON a.actor_id = fa.actor_id
    LEFT JOIN film AS f ON fa.film_id = f.film_id
    GROUP BY a.actor_id
)
SELECT
    actors_name,
    years_without_acting,
    latest_film_year
FROM ActorsPeriodWithoutActing
WHERE
    years_without_acting > 5
    AND rnk <= 4
ORDER BY years_without_acting DESC;